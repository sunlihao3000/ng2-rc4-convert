
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import {FbaModule} from './fba/fba.module';
import {FbaServiceModule} from './services/fba.service.module';


@NgModule({
  imports:      [ BrowserModule, FbaModule, FbaServiceModule.forRoot()],
  declarations: [ AppComponent ],

  bootstrap:    [ AppComponent ]
})

export class AppModule { }
