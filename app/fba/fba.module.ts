import {NgModule,forwardRef}  from "@angular/core";
import {FbaComponent} from './fba.component';
import { EqualValidator } from '../equal-validator.directive';
import { NG_VALIDATORS,FormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [CommonModule,FormsModule],
    declarations: [FbaComponent,EqualValidator],
    providers: [
        {provide: NG_VALIDATORS,  useExisting: forwardRef(() => EqualValidator), multi: true},
  ],
    exports: [FbaComponent,CommonModule],

})
export class FbaModule{}