import {Component,OnInit} from "@angular/core";
import {FbaService} from '../services/fba.service'; 
import { User } from '../user.interface';


@Component({
  selector: 'app-fba',
  templateUrl: './app/fba/fba.component.html',
  styleUrls:  ['./app/fba/fba.component.css'],

})

export class FbaComponent implements OnInit {  
    public user: User;
    // facebook accounts
    fbas;
    //
    inputText;
    //
    inputUser;
    //
    appState;
    //
    oldInputText;
    //
    popupState = 'hide';
    //
    displayClass = '';
    //
    top = 30;
 
    constructor( private _fbaService:FbaService){

    }

   ngOnInit() {
    this.fbas = this._fbaService.getFbas();
    this.user = {
        title:'',
        username: '',
        phone:'',
        email: ''
      }  
  }   

  //
  initPopUpWindow(theAppState){
    this.popupState = 'show';
    this.appState = theAppState;
    this.displayClass = 'show';
    this.top = 50;
  }
  //
  closePopUp(){
    this.popupState = "hide";
    this.displayClass = '';
    this.top = 30;
  }

  addNewFaceBookAccount(model: User, isValid: boolean) {
    // call API to save customer
    //console.log(model, isValid);

      var newFba = {
        title: model.title,
        username: model.username,
        phone:model.phone,
        email:model.email

      }

      this.fbas.push(newFba);

      this._fbaService.addNewFba(newFba);

      this.popupState = "hide";
      this.displayClass = '';
      this.top = 30;


  }

  //
  delFaceBookAccount(fbaText){
      for (var i = 0; i < this.fbas.length; i ++){
          if( this.fbas[i].title == fbaText ) {
            this.fbas.splice(i,1);
          }
      }
      this._fbaService.delFba(fbaText);
  }

  //
  editFacebookAccount(fba){
      this.appState = 'edit';
      this.popupState = 'show';
      this.displayClass = 'show';
      this.top = 50;
      console.log(fba);
      this.oldInputText = fba.title;
      this.user.title = fba.title;
      this.user.username = fba.username;
      this.user.phone = fba.phone;
      this.user.email = fba.email;
      console.log(this.appState);

      
  }

  //  
  updateFaceBookAccount(model: User, isValid: boolean){
    console.log(this.oldInputText);
    for (var i = 0; i < this.fbas.length; i ++){
        if( this.fbas[i].title == this.oldInputText ) {
          this.fbas[i].title = model.title;
          this.fbas[i].username = model.username;
          this.fbas[i].phone = model.phone;
          this.fbas[i].email = model.email;
        }
    }

    this.popupState = "hide";
    this.displayClass = '';
    this.top = 30;  
    this._fbaService.updateFba(this.oldInputText, model.title, model.username,model.email);
  }



 }