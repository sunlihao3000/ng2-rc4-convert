import {NgModule} from '@angular/core';
import {FbaService} from './fba.service';


@NgModule({})

export class FbaServiceModule{
    static forRoot(){
        return {
            ngModule: FbaServiceModule,
            providers:[FbaService]
        }
    }

}

