import {Injectable} from '@angular/core';
import { Init} from './init-fbas';

@Injectable()

export class FbaService extends Init{
    
    constructor() {
        super();

        console.log(' facebook account service is working');
        // this load() is local in init-fbas.ts
        this.load();
    }

   getFbas(){
     // first get all the items in localStorage, then convert into array
     var fbas = JSON.parse(localStorage.getItem('fbas'));
     // return all the items
     return fbas;
   }


    // add function
   addNewFba(newFba){
     
     // first get all the items in localStorage, then convert into array  
     var fbas = JSON.parse(localStorage.getItem('fbas'));
     // add new fba from input field, push into array
     fbas.push(newFba);
     // in order to add into localStorage, we need stringify again the array
     localStorage.setItem('fbas', JSON.stringify(fbas));
   }
     

   //del function
   delFba(fbaText) {
    
    // first get all the items in localStorage, then convert into array     
    var fbas = JSON.parse(localStorage.getItem('fbas'));
    
    for (var i = 0; i < fbas.length; i ++){
          if( fbas[i].title == fbaText ) {
            fbas.splice(i,1);
          }
    } 

     localStorage.setItem('fbas', JSON.stringify(fbas));     
   }
   // 


   // update function
   updateFba(oldText,newText,newUserName,newEmail) {

    var fbas = JSON.parse(localStorage.getItem('fbas'));
    for (var i = 0; i < fbas.length; i ++){
          if( fbas[i].title == oldText ) {
            fbas[i].title = newText;
            fbas[i].username = newUserName;
            fbas[i].email = newEmail;
          }
      } 
     localStorage.setItem('fbas', JSON.stringify(fbas));     
   }
 


}